﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightsController : ControllerBase
    {
        private readonly IFlightService _flightService;

        public FlightsController(IFlightService flightService)
        {
            _flightService = flightService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var flights = (await _flightService.GetAllAsync()).Result
                .Select(x => new FlightDataTransferObject
                {
                    Id = x.Id,
                    ArrivalDateTime = x.ArrivalDateTime,
                    Code = x.Code,
                    FlightStatusId = (int)x.FlightStatusId,
                    DepartureDateTime = x.DepartureDateTime,
                    DestinationAirport = x.DestinationAirport,
                    OriginAirport = x.OriginAirport
                });

            return Ok(flights);
        }

        [HttpGet]
        [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorResponseActionResult))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        [Route("{originAirport}/{destinationAirport}")]
        public async Task<IActionResult> GetByOriginAndDestination(string originAirport, string destinationAirport)
        {
            var flight = await _flightService.GetByOriginAndDestinationAsync(originAirport, destinationAirport);

            if (flight.Result == null)
                return NoContent();
            else
                return Ok(flight);
        }

        [HttpPost]
        [Route("{flightId}/Passenger/{passengerId}")]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorResponseActionResult))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> PostPassengerToFlight(long flightId, string passengerId) 
        {
            var result = await _flightService.PostPassengerToFlight(flightId, passengerId);

            return Ok();
        }

        
    }
}
