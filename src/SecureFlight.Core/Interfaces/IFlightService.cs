﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService: IService<Flight>
    {
        Task<OperationResult<Flight>> GetByOriginAndDestinationAsync(string originAirport, string destinationAirport);
        Task PostPassengerToFlight(long flightId, string passengerId);
    }
}
