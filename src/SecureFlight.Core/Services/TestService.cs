﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecureFlight.Core.Services
{
    public class TestService : BaseService<Flight>, IService<Flight>
    {
        public TestService(IRepository<Flight> repository) : base(repository)
        {

        }
    }
}
