﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService: BaseService<Flight>, IFlightService
    {
        private readonly IRepository<Flight> repository;

        public FlightService(IRepository<Flight> repository): base(repository)
        {
            this.repository = repository;
        }

        public async Task<OperationResult<Flight>> GetByOriginAndDestinationAsync(string originAirport, string destinationAirport)
        {
            var flights = await repository.GetAllAsync();

            Flight flight = flights.FirstOrDefault(flight => flight.OriginAirport == originAirport 
                                        && flight.DestinationAirport == destinationAirport);

            return new OperationResult<Flight>(true, flight);
        }

        public async Task PostPassengerToFlight(long flightId, string passengerId)
        {
            var flight = await repository.GetAllAsync();

            flight.FirstOrDefault(flight => flight.Id == flightId);

            //TODO: Stopped right here.

            throw new NotImplementedException();
        }
    }
}
